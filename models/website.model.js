const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let WebsiteSchema = new Schema({
    name: {type: String, required: true},
    single_skeleton: {type: String},
    navpage_skeleton: {type: String},
    url : {type: String, required: true},
    last_crawl : {type: Date }
});


module.exports = mongoose.model('Website', WebsiteSchema);