const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ArticleSchema = new Schema({ 
    title: {type: String, required: true},
    excerpt: {type: String},
    content: {type: String, required: true},
    tags : {type: String},
    category : {type: String},
    thumbnail :{type: String},
    link :{type: String}, 
    source : {type: String},
    date : {type: String} ,  
});

module.exports = mongoose.model('Article', ArticleSchema);