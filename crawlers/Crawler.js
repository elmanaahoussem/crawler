const Leecher = require ('../Api/StreamingApi');
const debug = require('debug')('Crawler Module');
const cheerio = require('cheerio');
const request = require('request-promise');

const Logger = require('../controllers/Logger')
const Cosinesimilarity = require('../libs/cosinesimilarity');
const StreamingApi = require('../Api/StreamingApi')
const FrontApi = require('../Api/FrontApi')
const path = require("path");

const websiteModel = require('../models/website.model');
const videoModel = require('../models/article.model');

class Crawler {

    constructor (WebName,WebUrl,WebNavUrl,WebSingleUrl) {
        this.WebName = WebName;
        this.WebUrl = WebUrl;
        this.WebNavUrl = WebNavUrl;
        this.WebSingleUrl = WebSingleUrl  // temporary As Exemple to get Sekeleton
        this.logger = new Logger();
    }

    async init () {
        // Init Makes Sure that the Website is Created in BDD and loads all requirements
        let Website = await websiteModel.findOne({  url : this.WebUrl }).catch(e => { 
           this.logger.logError(this.WebName + ' Method Init Error : '   + e)    
            })
      
        if (! Website) { 
            Website = await this.createWebsiteEntry(); 
        } 
        this.WebId = Website._id
        this.single_skeleton = JSON.parse(Website.single_skeleton) 
        this.navpage_skeleton = JSON.parse(Website.navpage_skeleton) 
        this.last_crawl = Website.last_crawl
    }

    async createWebsiteEntry () {
        const Website = await websiteModel.create({
            name: this.WebName, 
            url : this.WebUrl,
            single_skeleton : JSON.stringify(await this.requestPageSkeleton(this.WebSingleUrl)),
            navpage_skeleton : JSON.stringify(await this.requestPageSkeleton(this.nextNavPage(1))),
            last_crawl : new Date()
        }).catch(e => { 
           this.logger.logError(this.WebName + ' Method createWebsiteEntry Error : '   + e)    
        }) 
        return Website 
    }

    async requestPageSkeleton (URL = this.WebUrl) {
        // Send Request to get The Website and gets the Body Skeleton
        var options = {
            uri: URL,
            transform:   (body) => { 
                const Skeleton = this.extractSkeleton(body)
                this.skeleton =  Skeleton 
                return Skeleton
            }
        };
        try {
            return await request(options)   
        }catch (e) {
              this.logger.logError(this.WebName+" requestPageSkeleton Request Failed "+" URL : "+URL +"  Error >>> "+e)
             return Promise.reject(this.WebName +' Request Page Skeleton Failed')
        } 
    }

    async requestPageBody (URL = this.WebUrl) {
        // Send Request to get The Website and gets the Body HTML
        var options = {
            uri: URL,
            transform:   (body) => { return  cheerio.load(body); }
        };

        try {
            return await request(options)   
        }catch (e) {
            this.logger.logError(this.WebName+" requestPageBody Request Failed "+e)
             return Promise.reject(this.WebName +' Request Page Body Failed')
        } 
       
    }

    async getPagedSkeleton (field) {
        let Website = await websiteModel.findById(this.WebId).catch(e =>{
            this.logger.logError(this.WebName + ' Method GetPagedSkeleton Error : '   + e)    
        }) 
       return JSON.parse(Website[field]) 
    }

    async saveWebsiteSkeleton (field , newSkeleton) {

        let Website = await websiteModel.findById(this.WebId).catch(e =>{
            this.logger.logError(this.WebName + ' Method saveWebsiteSkeleton Error : '   + e)    
        }) 

        if (! Website)
        Website = await this.createWebsiteEntry();

        let data = {}
        data[field]  = JSON.stringify(newSkeleton),
        data.last_crawl = new Date()   
        const result = await websiteModel.updateOne( 
            { _id: this.WebId }, data, {upsert : true})
            .catch (e =>{     this.logger.logError(this.WebName + ' Method saveWebsiteSkeleton UPDATELEVEL Error : '   + e )     } ) 

        return result 
    }

    async updateVideoField (videoId,field,value) {
        let data = {}
        data[field]  = value
        const result = await videoModel.findOneAndUpdate (
            { _id: videoId }, data, {upsert : true ,new: true })
            .catch (e =>{     this.logger.logError(field + ' -- Field  : updateVideoField Error : '   + e )     } )

        return result
    }

    extractSkeleton (html) {
        // Return Page Skeleton as Array
        const $ = cheerio.load(html);  
        const siteElements  = $("body *");
    
        let siteStructure = [];     
        siteElements.map(function (k) { 
            siteStructure[k] = new Object;
            siteStructure[k].name =  $(this).prop("tagName");
            siteStructure[k].class =  $(this).attr("class");
            siteStructure[k].style =  $(this).attr("style");
            siteStructure[k].id =  $(this).attr("id");
        })
        return siteStructure  
    }

    compareSkeletons(skeleton1,skeleton2) {
        if (skeleton1.length !== skeleton2.length)
        return false;
        for (let i=0 ; i< skeleton1.length ; i++)  {
              
            if (skeleton1[i].name !== skeleton2[i].name)
            { 
                debug("Order Tags Changed in" + i + "Element");
                return false
            }
            if ( skeleton1[i].class !== skeleton2[i].class)
            { 
                debug("class Changed in" + skeleton1[i].name + "Element , Tag Number " + i );
                return false
            }

            if (skeleton1[i].style !== skeleton2[i].style)
            {
                debug("Style Changed in" + skeleton1[i].name + "Element , Tag Number " + i );
                return false
            }
            
            if (skeleton1[i].id !== skeleton2[i].id)
            {
                debug("Id Changed in" + skeleton1[i].id + "Element , Tag Number " + i );
                return false
            }
        }  
        return true
    }

    async WebsiteTestSkeleton () { 
        const latestSingle = await this.requestPageSkeleton(this.WebSingleUrl)
        const latestNav = await this.requestPageSkeleton(this.nextNavPage(1))
        const savedSingle = await this.getPagedSkeleton("single_skeleton")
        const savedNav = await this.getPagedSkeleton("navpage_skeleton") 
        let $ = null
     
        if (!this.compareSkeletons(latestSingle,savedSingle))
        {
            $ = await this.requestPageBody(this.WebSingleUrl)
            return this.validateSingleMainElements($,latestSingle)
        } 
        if (!this.compareSkeletons(latestNav,savedNav))
        {
            $ = await this.requestPageBody(this.nextNavPage(1))
            return this.validateNavMainElements($,latestNav)
        }  
        return true
    }

    async saveVideo (NewVideo) {
        const Video = await videoModel.create(NewVideo).catch (e =>{this.logger.logError(this.WebName + ' Method saveVideo Error : '   + e )}) ;
        return Video
    }

    arrayToJson (theArray) {
        let final = [];
        for (let i =0 ; i<theArray.length ;i++)
        {
            final.push(theArray[i])
        }
        return JSON.stringify(final)
    }

    async checkVideoInLibrary (videoName) {
        const CosineSimilarity = new Cosinesimilarity();
        let data = await videoModel.aggregate([
            {
                "$match": {
                    "$text": {
                        "$search":  videoName
                    }
                }
            },
            {
                "$project": {
                    "name":1,
                    "_id": 1,
                    "score": {
                        "$meta": "textScore"
                    }
                }
            },
            {
                "$match": {
                    "score": { "$gt": 1.0 }
                }
            },
            {
                "$sort": {
                    "score": { "$meta": "textScore" }
                }
            },
            {
                "$limit": 1
            }
        ] )

        if (data[0]) {
             if (CosineSimilarity.compare(data[0].name,videoName) > 0.6){
                this.logger.logWarning(this.WebName + ' Duplicated Video : '   + videoName)
                return true
            }
        }

        return false

    }

    async getLatestVideos (options = null) {

        const webtest = await this.WebsiteTestSkeleton();

        if (!webtest){
            this.logger.logError(this.WebName + " GetLatest Videos WebsitestSkeleton Failed")
            return Promise.reject('WebSkeleton Verification Failed')
        }

        options = {
            numberOfVideos : options.numberOfVideos?options.numberOfVideos:10,
            videosTillDate : options.videosTillDate?new Date(options.videosTillDate):new Date(),
            videosFromDate : options.videosFromDate?new Date(options.videosFromDate):null,
        }
        this.currentNavPage = options.startPage?options.startPage:1


        let videosList = [];
        let Singlecontent = null;
        let singleUrls = await this.crawlNavPages(options)
        let isInLibrary = null
        let insertedCount = 0;

        for(let i = 0 ; i< singleUrls.length && i< options.numberOfVideos ; i++) {
            try {
                Singlecontent =  await this.crawlSingle(singleUrls[i])
            }catch (e) {
                this.logger.logError(this.WebName+" getLatestVideos Failed "+e)
                break;
            }
            isInLibrary = await this.checkVideoInLibrary(Singlecontent.name)
            if (!isInLibrary){
                //await this.saveVideo(Singlecontent)
                await this.SubmitVideo(Singlecontent)
                insertedCount++;
            }

        }
        return Promise.resolve({ website : this.WebName, inserted : insertedCount  });
    }

    async crawlNavPages (options) {
        let AllUrls = []
        let SingleUrls = []
        let i

        do {
            try {
                SingleUrls  = await this.crawlNavPage(this.nextNavPage(),options.videosTillDate,options.videosFromDate)
            }catch (e) {
                this.logger.logError(this.WebName+" crawlNavPages Failed "+e)
                break;
            }

        } while(SingleUrls.length === 0)

        for (i =0 ; i<SingleUrls.length ; i++ )
        {
            AllUrls.push(SingleUrls[i])
        }

        do {
            SingleUrls =[]
            try {
                SingleUrls  = await this.crawlNavPage(this.nextNavPage(),options.videosTillDate,options.videosFromDate)
            }catch (e){
                this.logger.logError(this.WebName+" crawlNavPages Failed "+e)
                break;
            }
            for (i =0 ; i<SingleUrls.length ; i++ )
            {
                AllUrls.push(SingleUrls[i])
            }
        }while(SingleUrls.length > 0)

        return AllUrls
    }


    getSourceVideo () {
        // downloads Video from k2s returns URL on the host server
        return 'temp/1.mp4'
    }

     async uploadVideoToHosts (VideoUri,video) {
        // Upload The Downloaded Video to hosts
        // then deletes video from Host Server
        // returns video Object with Complete links

         /*
        const Api = new StreamingApi('https://gounlimited.to/','25974naptk8fp1at44h61');
         console.log('Start Upload to Stream')
        let gounlimitedlink = await Api.uploadFile(VideoUri)
         console.log('Done Upload to Stream')
          */
        video = await this.updateVideoField(video._id,"gounlimited","somelinkto")
         console.log('Updated Field')
         return video


    }

    async SubmitVideo(video) {
        // Submits the Video To the WebSite
        // Recieves the video ID and Link
        const videoUri = this.getSourceVideo()
        video = await this.saveVideo(video)
        console.log('Video Saved')
        video = await this.uploadVideoToHosts(videoUri,video)
        console.log('Start Uploading to API' , video)
        const rslt = await FrontApi.submitVideo(video)
        return rslt
    }

 
}

module.exports = Crawler;