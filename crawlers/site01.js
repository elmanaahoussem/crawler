const Crawler = require('./Crawler');
const cheerio = require('cheerio');
const request = require('request-promise');


class site01 extends Crawler {

    constructor () {
        super ("site01",
            "http://site01.com",
            "http://site01.com/Navpage/",
            "http://site01.com/exemple-single/")
        this.currentNavPage = 1
    }

    nextNavPage (page = null) {
        if(page){
            return this.WebNavUrl + page //Get A Specific Page Nav Url
        }
        return this.WebNavUrl + this.currentNavPage++
    }

    async crawlNavPage ( navUrl,videosTillDate = new Date(),videosFromDate = null) {
        //console.log(navUrl)
       // console.log("videosTillDate : " + videosTillDate + " videosFromDate : " + videosFromDate)
        let SingleUrls = []
        let varDate = null
        var options = {
            uri: navUrl,
            transform:   (body) => {
                const $ = cheerio.load(body);
                SingleUrls  = $('.content-area article').map( function () {
                    varDate =   $(this).find('time').text()
                    if( (new Date (varDate ) <= videosTillDate) && (videosFromDate?(new Date (varDate ) >= videosFromDate):true) )
                        return $(this).find('.entry-title a').attr('href')
                })
               // console.log('fetched '+ SingleUrls.length)
                return SingleUrls
            }
        };
        try {
            return await request(options)
        }catch (e) {
            this.logger.logError(this.WebName+" crawlNavPage Request Failed "+e +" URL : "+url)
            return Promise.reject(this.WebName +' crawlNavPage Failed')
        }
    }

    async validateNavMainElements ($,latestSkeleton) {
        const links = $('.content-area article').length > 0
        if (links) {
            await this.saveWebsiteSkeleton("navpage_skeleton",latestSkeleton)
            return true
        }
        this.logger.logWarning(this.WebName,"validateNavMainElements","One Of The Main Inputs Missing")
        return false
    }

    async crawlSingle (url = this.WebSingleUrl) {
        let videoObject = {
            name: null,
            description: null,
            tags : null,
            category : null,
            thumbnail : null,
            link : null,
            source : this.WebName,
            date : null ,
        }
        var options = {
            uri: url,
            transform:   (body) => {
                const $ = cheerio.load(body);
                videoObject.name = $("h1.entry-title").text().trim();
                videoObject.description = ""
                videoObject.category = "cat"
                videoObject.thumbnail = $(".entry-content img").first().attr('src')
                videoObject.link = $(".entry-content p > a[href*='k2s']").first().attr('href')
                videoObject.date = new Date($('time').text().trim())

                return videoObject
            }
        };
        try {
            return await request(options)
        }catch (e) {

            this.logger.logError(this.WebName+" crawlSingle Request Failed "+e +" URL : "+url)
            return Promise.reject(this.WebName +' crawlSingle Failed')
        }
    }

    async validateSingleMainElements ($,latestSkeleton) {
        const title = $("h1.entry-title").length  > 0
        const thumbnail = $(".entry-content img").length > 0
        const link = $(".entry-content p > a[href*='k2s']").length > 0
        const date = $('time').length > 0
        if (  title   && link && date && thumbnail) {
            await this.saveWebsiteSkeleton("single_skeleton",latestSkeleton)
            return true
        }
        this.logger.logWarning(this.WebName,"validateSingleMainElements","One Of The Main Inputs Missing")
        return false
    }


}

module.exports = site01;