const Website = require('../models/website.model');

/**
 * Controller URL : '/websites'
 * 
 * 
 * iwas in the step to see how i need to fill body 
 */

/* URL : '/add' */
exports.add_website = function (req, res,next) {
    let website = new Website(
        {
            name: req.body.name,
            body: req.body.body,
            url : req.body.url,
            last_crawl : req.body.date
        }
    ); 
    website.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('website Created successfully')
    })
};
/* URL : '/:id' */
exports.get_website = function (req, res,next) {
    Website.findById(req.params.id, function (err, website) {
        if (err) return next(err);
        res.send(website);
    })
};
/* URL : '/:id/update' */
exports.update_website = function (req, res,next) {
    Website.updateOne({ _id: req.params.id}, {$set: req.body}, function (err, Website) {
        if (err) return next(err);
        res.send(Website);
    });
};
/* URL : '/:id/delete' */
exports.delete_website = function (req, res,next) {
    Website.deleteOne({ _id: req.params.id}, function (err) {
        if (err) return next(err);
        res.send('Deleted successfully!');
    })
};