const Article = require('../models/article.model');
 
exports.add_article = function (req, res,next) {
    let articleInstance = new Article(
        { 
            title : req.body.title,
            excerpt :  req.body.excerpt??'N/A',
            content :  req.body.content,
            tags :  req.body.tags??'N/A',
            category :  req.body.category??'N/A',
            thumbnail :  req.body.thumbnail??'N/A',
            link :  req.body.link??'N/A',
            source :  req.body.source??'N/A',
            date :  req.body.date??'N/A',
        }
    );
    articleInstance.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Article Created successfully')
    })
};

exports.get_article = function (req, res,next) {
    article.findById(req.params.id, function (err, article) {
        if (err) return next(err);
        res.send(article);
    })
};

exports.update_article = function (req, res,next) {
    article.updateOne({ _id: req.params.id}, {$set: req.body}, function (err, article) {
        if (err) return next(err);
        res.send(article);
    });
};

exports.delete_article = function (req, res,next) {
    article.deleteOne({ _id: req.params.id}, function (err) {
        if (err) return next(err);
        res.send('Article deleted successfully!');
    })
};