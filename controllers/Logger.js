var fs = require('fs')
 
class Logger {

     constructor () {
        this.errors = fs.createWriteStream('logs/errors.txt', { flags: 'a'})
        this.crawllog = fs.createWriteStream('logs/crawllog.txt', { flags: 'a'})
        this.warningslog = fs.createWriteStream('logs/warnings.txt', { flags: 'a'})
    }

    logError( logs) { 
        this.errors.write(this.getTime() +' '+logs+'\n')
    }

    logCrawl(logs) {
        this.crawllog.write(this.getTime() +' '+ logs.website +' Inserted '+ logs.inserted+' Videos \n')
    }

    logWarning(logs) {
       this.warningslog.write(this.getTime() +' '+logs+'\n')
       // to close the writestream
    }
    getTime () {
        let currentdate = new Date()
        return  currentdate.getFullYear() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getDate() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds() + " >>>>>> ";
    }
}



module.exports = Logger;