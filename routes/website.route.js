const express = require('express');
const router = express.Router();

 
const website_controller = require('../controllers/website.controller');
 

router.post('/add', website_controller.add_website);

router.get('/:id', website_controller.get_website);

router.put('/:id/update', website_controller.update_website);

router.delete('/:id/delete', website_controller.delete_website);



module.exports = router;