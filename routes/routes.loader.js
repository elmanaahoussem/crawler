const express = require('express'); 
const bodyParser = require('body-parser');
const website = require('./website.route');  
const article = require('./article.route');  
 
module.exports = function loadroutes () {
    const app = express(); 
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));
    app.use('/websites', website);
    app.use('/articles', article);
    return app
}