const express = require('express');
const router = express.Router();

 
const article_controller = require('../controllers/article.controller');


router.post('/add', article_controller.add_article);

router.get('/:id', article_controller.get_article);

router.put('/:id/update', article_controller.update_article);

router.delete('/:id/delete', article_controller.delete_article);


module.exports = router;



