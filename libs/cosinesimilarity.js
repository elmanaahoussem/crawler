const debug = require('debug')('Cosinesimilarity Module')

class Cosinesimilarity {

    constructor ( ) { 
    }

    termFreqMap(str) {
        let words = str.split(' ');
        let termFreq = {};
        words.forEach(function(w) {
            termFreq[w] = (termFreq[w] || 0) + 1;
        });
        return termFreq;
    }

    addKeysToDict(map, dict) {
        for (let key in map) {
            dict[key] = true;
        }
    }

    termFreqMapToVector(map, dict) {
        let termFreqVector = [];
        for (let term in dict) {
            termFreqVector.push(map[term] || 0);
        }
        return termFreqVector;
    }

    vecDotProduct(vecA, vecB) {
        let product = 0;
        for (let i = 0; i < vecA.length; i++) {
            product += vecA[i] * vecB[i];
        }
        return product;
    }

    vecMagnitude(vec) {
        let sum = 0;
        for (let i = 0; i < vec.length; i++) {
            sum += vec[i] * vec[i];
        }
        return Math.sqrt(sum);
    }

     Similarity(vecA, vecB) {
        return this.vecDotProduct(vecA, vecB) / (this.vecMagnitude(vecA) * this.vecMagnitude(vecB));
    }

    adabtTitle (title) {
        const ignoreList = ["hd","fullhd","sd","1080p","720p","480p","360p","mp4","wmv"]
        const expStr = ignoreList.join("|");
        return title.replace(new RegExp('\\b(' + expStr + ')\\b', 'gi'), ' ').replace(/\s{2,}/g, ' ').replace(/ *\([^)]*\) */g, "").replace(/\[+([^\][]+)]+/g, "").trim();
    }

      compare (strA,strB) {
        let termFreqA = this.termFreqMap(this.adabtTitle(strA));
        let termFreqB = this.termFreqMap(this.adabtTitle(strB));
   
        let dict = {};
        this.addKeysToDict(termFreqA, dict);
        this.addKeysToDict(termFreqB, dict);

        let termFreqVecA = this.termFreqMapToVector(termFreqA, dict);
        let termFreqVecB = this.termFreqMapToVector(termFreqB, dict);
 
        return  this.Similarity(termFreqVecA, termFreqVecB);
    }
}

module.exports = Cosinesimilarity;

 

 
 